import { RESET_ALERT, SET_ALERT } from "../../constants/ActionTypes";

export const setAlert = (alertType, message) => {
  console.log("inside action setAlert");
  return {
    type: SET_ALERT,
    payload: {
      type: alertType,
      message: message,
    },
  };
};

export const resetAlert = () => {
  console.log("inside action setAlert");
  return {
    type: RESET_ALERT,
    payload: null,
  };
};
