import { DOWNLOAD_CHANNEL } from "../../constants/ActionTypes";

export const channelReducer = (zipcode = {}, action) => {
  switch (action.type) {
    case DOWNLOAD_CHANNEL: {
      console.log(DOWNLOAD_CHANNEL);
      return action.payload;
    }
    default:
      return zipcode;
  }
};
