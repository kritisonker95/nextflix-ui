import axios from "axios";

const NextflixApi = axios.create({
  baseURL: "http://localhost:8080/api/v1",
  headers: {
    Accept: "application/json",
  },
});

export default NextflixApi;
