import React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";
import ChannelHeader from "./ChannelHeader";
import ChannelBody from "./ChannelBody";
import { connect } from "react-redux";
import PdfDownload from "../Download/PdfDownload";

const columns = [
  { id: "channelNumber", label: "Number", minWidth: 170, align: "left" },
  { id: "channelName", label: "Channel", minWidth: 100, align: "left" },
  {
    id: "standard",
    label: "Standard",
    minWidth: 170,
    align: "center",
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "preferred",
    label: "Preferred",
    minWidth: 170,
    align: "center",
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "basic",
    label: "Basic",
    minWidth: 170,
    align: "center",
    format: (value) => value.toFixed(2),
  },
];

const Channels = (props) => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const tableContainerSX = {
    maxHeight: 400,
    tableLayout: "fixed",
    whiteSpace: "nowrap",
    overflow: "scroll",
    backgroundColor: "var(--table-data-color)",
  };

  const tablePaginationSX = {
    "& p": {
      marginTop: "12px",
      marginBottom: "1rem",
    },
    backgroundColor: "var(--table-pagination-color)",
  };

  return (
    <div className="table-container">
      {props.zipcode &&
      props.zipcode.channels &&
      props.zipcode.channels.length !== 0 ? (
        <div>
          <Paper
            sx={{ marginLeft: "2rem", marginRight: "2rem", overflow: "hidden" }}
          >
            <TableContainer sx={tableContainerSX}>
              <Table stickyHeader aria-label="sticky table">
                <ChannelHeader className="table-header" columns={columns} />
                <ChannelBody
                  rows={props.zipcode.channels}
                  columns={columns}
                  page={page}
                  rowsPerPage={rowsPerPage}
                />
              </Table>
            </TableContainer>
            <TablePagination
              sx={tablePaginationSX}
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={props.zipcode.channels.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
          <PdfDownload />
        </div>
      ) : (
        <div style={{ height: "37vh" }}></div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  console.log("Channels mapStateToProps");
  console.log(state);
  return {
    zipcode: state.zipcode,
  };
};

export default connect(mapStateToProps)(Channels);
