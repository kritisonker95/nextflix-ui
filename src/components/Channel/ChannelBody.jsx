import React from "react";
import { TableBody } from "@mui/material";
import Channel from "./Channel";

const ChannelBody = ({ rows, columns, page, rowsPerPage }) => {
  return (
    <TableBody
      sx={{
        margin: "0rem 1rem 0rem 1rem",
      }}
    >
      {rows
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row) => {
          return <Channel row={row} columns={columns} />;
        })}
    </TableBody>
  );
};

export default ChannelBody;
