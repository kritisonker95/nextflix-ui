import { useRef } from "react";
import React from "react";
import { connect } from "react-redux";
import { downloadChannel } from "../../redux/actions/ChannelAction";
import "./PdfDownload.css";
import { Tooltip } from "@mui/material";
import { DOWNLOAD_CHANNEL_BTN_TOOLTIP } from "../../constants/StringConstants";

const PdfDownload = (props) => {
  const downloadRef = useRef(null);
  const url = `data:application//octet-stream;base64,${props.file.base64}`;
  const fileName = `${props.file.fileName}`;
  console.log("PdfDownload props");
  console.log(props);

  const onDownload = (e) => {
    e.preventDefault();
    props.downloadChannel(props.zipcode.zipcode);
    setTimeout(() => {
      downloadRef.current.click();
    }, 1000);
  };

  return (
    <div className="download" data-testid='download-button'>
      <a
        ref={downloadRef}
        style={{
          background: "brown",
          position: "absolute",
          marginRight: "0.5rem",
        }}
        href={url}
        download={fileName}
      ></a>
      <Tooltip
        title={`${DOWNLOAD_CHANNEL_BTN_TOOLTIP}: ${props.zipcode.zipcode}`}
        arrow
      >
        <input
          className="btn-default"
          value="Download PDF"
          onClick={onDownload}
        />
      </Tooltip>
    </div>
  );
};

const mapStateToProps = (state) => {
  console.log("PdfDownload mapStateToProps");
  console.log(state);
  return {
    zipcode: state.zipcode,
    file: state.file,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    downloadChannel: (zipcode) => dispatch(downloadChannel(zipcode)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PdfDownload);
