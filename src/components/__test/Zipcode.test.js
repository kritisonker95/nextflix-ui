import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { render, cleanup,screen, fireEvent} from "@testing-library/react";
import { reducers } from "../../redux/reducers";
import Zipcode from "../Zipcode/Zipcode";
import userEvent from "@testing-library/user-event";




const initialState = {};

//for redux
const renderWithRedux = (
  component,
  { initialState, store = createStore(reducers, initialState) } = {}
) => {
  return {
    ...render(<Provider store={store}>{component}</Provider>),
    store,
  };
};

afterEach(cleanup);

//Check whether the textbox for Zipcode input is present 
it("render text input",() => {
  const { getByTestId } = renderWithRedux(<Zipcode />);
  expect(getByTestId('textbox')).toBeInTheDocument();
});


//Check for the input type of textbox is text only
it("should display type of textbox", () => {
  const { getByTestId } = renderWithRedux(<Zipcode />);
  expect(getByTestId('textbox')).toHaveAttribute('type','text');
});

//Check for the placeholder of Zipcode textbox
it("should have placeholder 'Enter Zipcode'",()=>{
  const { getByTestId } = renderWithRedux(<Zipcode />);
  expect(getByTestId('textbox')).toHaveAttribute('placeholder', 'Enter Zipcode') ;
});

// Check for hover over Zipcode textbox
it('hover input textbox', () => {

  const { getByTestId } = renderWithRedux(<Zipcode />);
  userEvent.hover(getByTestId('textbox'));
  expect(getByTestId('textbox')).toBeTruthy();
  });

//Check for tooltip availability on mouse over on button(Find Channels)
it('tooltip appears on button hover',() => {
  renderWithRedux(<Zipcode/>);
  userEvent.hover(screen.getByTestId('find-button'))
  expect(screen.getByTestId('button-tooltip')).toBeInTheDocument();
});

//Check for button text (Find Channels)
it('Button shows text - Find Channels',() => {
    renderWithRedux(<Zipcode />);
    expect(screen.getByRole('button')).toHaveTextContent('Find Channels');
});

//Check for valid zipcode entry , button to be enabled and button click
it('pass valid zipcode to test Zipcode input field', async () => {
  
  renderWithRedux(<Zipcode />);
  const inputEl = screen.getByTestId('textbox');
  userEvent.type(inputEl, '110092');
  expect(screen.getByTestId('textbox')).toHaveValue('110092');
  expect(screen.queryByTestId('find-button')).not.toBeDisabled();
  userEvent.click(screen.queryByTestId('find-button'));
  expect(screen.queryByTestId('find-button')).toBeValid();
});

//Check for invalid Zipcode entry , button to be disabled for the same and
//for no input also button should be disabled
it('pass invalid zipcode to test Zipcode input field',() => {
  renderWithRedux(<Zipcode />);

  const inputEl = screen.getByTestId('textbox');
  userEvent.type(inputEl, 'invalidtext12$%---   ');
  expect(screen.getByTestId('textbox')).toHaveValue('invalidtext12$%---   ');
  expect(screen.queryByTestId('find-button')).toBeDisabled();
  userEvent.clear(inputEl);
  expect(screen.queryByTestId('find-button')).toBeDisabled();

});





