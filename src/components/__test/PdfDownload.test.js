import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { render, cleanup,screen, getByTestId} from "@testing-library/react";
import PdfDownload from "../Download/PdfDownload";
import userEvent from "@testing-library/user-event";
import { reducers } from "../../redux/reducers";


const initialState = {};


//for redux
const renderWithRedux = (
  component,
  { initialState, store = createStore(reducers, initialState) } = {}
) => {
  return {
    ...render(<Provider store={store}>{component}</Provider>),
    store,
  };
};

afterEach(cleanup);


//Check for the availability of button (Download Pdf)
it("render pdf download button",() => {
  const { getByTestId } = renderWithRedux(<PdfDownload />);
  expect(getByTestId('download-button')).toBeInTheDocument();
});

//Check for tooltip availability on mouse over on button(Download Pdf)
it("display tooltip on hovering the download button",() => {
  renderWithRedux(<PdfDownload/>);
  userEvent.hover(screen.getByTestId('download-button'))
  expect(screen.getByTestId('download-button-tooltip')).toBeInTheDocument();
});

