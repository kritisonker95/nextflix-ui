import { Tooltip } from "@mui/material";
import React from "react";
import { useState } from "react";
import { connect } from "react-redux";
import {
  ENTER_ZIPCODE_BTN_TOOLTIP,
  FIND_CHANNEL_BTN_TOOLTIP,
} from "../../constants/StringConstants";
import { fetchZipcode } from "../../redux/actions/ZipcodeAction";
import "./Zipcode.css";

const Zipcode = (props) => {
  const [zipcode, setZipcode] = useState("");
  const [isDisabled, setDisabled] = useState(true);

  const stringValPatternValidation = (stringVal) => {
    return stringVal === "" || /\s/g.test(stringVal);
  };

  const onChangeZipcode = (e) => {
    e.preventDefault();
    let zipText = e.target.value;
    setZipcode(zipText);
    // Validating input zipcode
    if (stringValPatternValidation(zipText)) {
      setDisabled(true);
    } else if (isNaN(zipText)) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  };

  const onFindChannels = (e) => {
    e.preventDefault();
    props.fetchZipcode(Number(zipcode));
  };

 
  // Tooltip title
  function tooltipTitle() {
    return zipcode !== undefined &&
      !isNaN(zipcode) &&
      !stringValPatternValidation(zipcode)
      ? `${FIND_CHANNEL_BTN_TOOLTIP} : ${zipcode}`
      : ENTER_ZIPCODE_BTN_TOOLTIP;
  }

  return (
    <div className="zipcode">
      <Tooltip data-testid='button-tooltip' title={tooltipTitle()} arrow>
        <span>
          <button
            className={isDisabled ? "btn-disable" : "btn-default"}
            data-testid="find-button"
            onClick={onFindChannels}
            disabled={isDisabled}
          >
            Find Channels
          </button>
        </span>
      </Tooltip>
      <input
        className="channel-textbox"
        data-testid="textbox"
        type="text"
        value={zipcode}
        placeholder="Enter Zipcode"
        onChange={onChangeZipcode}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  console.log("Zipcode mapStateToProps");
  console.log(state);
  return {
    zipcode: state.zipcode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchZipcode: (zipcodeId) => dispatch(fetchZipcode(zipcodeId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Zipcode);
